'use strict';

var SearchModel = function (keywords, lang, occurrences) {
  this.keywords = keywords;
  this.lang = lang;
  this.occurrences = occurrences;
};


var OpenSearchFeedModel = {
  'xmlns' : 'http://www.w3.org/2005/Atom',
  'title' : '',
  'link' : {  
    href : '',
    rel : 'alternate'
  },
  'id' : '',
  'updated' :'',
  'entries' : []
}

function OpenSearchEntryModel(title, link, updated, id, summary) {
 this.title = title.replace(/&/g, '&amp;');
 this.link = {
    href : link.href,
    rel : 'alternate'
  };
  this.updated = updated;
  this.id = id;
  this.summary = {
    summary : summary.summary.replace(/&/g, '&amp;'),
    type : summary.type
  }
}

var OpenSearchDescriptor = {
  'shortName' : 'Mediacenter Search',
  'description' : 'Search API to go through mediacenter content.',
  'url' : { 
    'type': 'application/atom+xml', 
    'template':'//api/mediacenter/v1.0/opensearch/{searchTerms}'
  }
}

exports.SearchModel = SearchModel;
exports.OpenSearchFeedModel = OpenSearchFeedModel;
exports.OpenSearchEntryModel = OpenSearchEntryModel;
exports.OpenSearchDescriptor = OpenSearchDescriptor;
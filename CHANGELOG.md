# Changelog

## v1.0.0 (2020-12-23)

- Allows`.epub` extension in mediacenter
- Add a release job to create a release on each pushed tag

## v0.0.9 (2020-12-17)

### Added

- Theses changes allow to count the number of image (filetype `jpg`, `png`) in `/data/content/`
- This allow to display image content also when there is only JPG or PNG
  file for the content
- PDF are not displayed anymore, an image for the PDF is displayed with a
  link to download the PDF file
- Refactor `check-for-new-content.sh` in order to avoid useless rebuild at
  start time of the app
- Added build time metrics for template

### Changed

- JSON is parsed preferably to filesystem for better parsing
- Javascript Dom has been used in order to display content other than JPG/ PNG
- Added some margin around download button
- Revert refactoring without JS branch counterproductive state
- Disable sitemap

### Removed

- Parsing data from filesystem in `/data/content`
- Removed relative content search in `page-search.html`

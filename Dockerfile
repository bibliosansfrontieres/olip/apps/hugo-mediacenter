# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-armv7l.tar.gz' ;; \
		amd64) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-x64.tar.gz' ;; \
		i386) node_build='https://unofficial-builds.nodejs.org/download/release/v13.8.0/node-v13.8.0-linux-x86.tar.gz' ;; \
		arm64) node_build='https://nodejs.org/dist/v13.8.0/node-v13.8.0-linux-arm64.tar.gz' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet $node_build -O- | tar --strip=1 -C /usr/local -xzf -

# hadolint ignore=DL3008,DL3015
RUN set -eux; \
	ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	if [[ "$ARCH" = "armhf" ]]; then \
		apt-get update; \
		apt-get install --yes --quiet --quiet libatomic1 \
			&& apt-get clean \
			&& rm -rf /var/lib/apt/lists/*; \
	fi

ENV HUGO_BASE_URL http://localhost:1313

WORKDIR /hugo

COPY api ./api
COPY api/app.js ./

COPY package.json ./

RUN ARCH="$(dpkg --print-architecture | awk -F- '{ print $NF }')" && \
	case $ARCH in \
		armhf) arch='ARM' ;; \
		amd64) arch='64bit' ;; \
		i386) arch='32bit' ;; \
		arm64) arch='ARM64' ;; \
		*) echo >&2 "error: unsupported architecture: $ARCH"; exit 1 ;; \
	esac && \
	wget --quiet "https://github.com/gohugoio/hugo/releases/download/v0.58.3/hugo_0.58.3_Linux-$arch.tar.gz"  -O- \
	| tar xz -C /usr/local/bin hugo && \
	npm install

COPY entrypoint.sh /
COPY hugo-config /hugo-config

COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY scripts/* /usr/local/bin/

EXPOSE 3000

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/bin/supervisord"]

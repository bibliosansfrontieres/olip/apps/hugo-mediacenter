#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') install_content.sh: $*"
}

say "Package added: $1"

if [ -d "/data/$1" ]
then
    say "> Package $1 has a previous build on disk. Checking whether it differs..."

    total_folder_builded=$(find "/data/$1" -maxdepth 1 ! -name page  -type d | wc -l)
    total_folder_needed=$(find "/data/content/$1" -maxdepth 1 ! -name index.md  -type f -name "*.md" | wc -l)

    if [[ "$total_folder_builded" != "$total_folder_needed" ]]
    then
        say "> > Package $1 differs, rebuild mediacenter"
        hugo-build
    else
        say "> > Package $1 is complete, nothing to do"
    fi
else
    say "> No previous build found for $1 , rebuild mediacenter"
    hugo-build
fi
